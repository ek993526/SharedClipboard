package com.ek.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
public class EncryptedData implements Serializable {
    @Serial
    private static final long serialVersionUID = 4567890324L;

    private byte[] data;
    private byte[] additionalData;
}
