package com.ek.service;

import lombok.SneakyThrows;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.util.Optional;

public class ClipboardService {

    public void set(String data) {
        StringSelection stringSelection = new StringSelection(data);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }


    @SneakyThrows
    public Optional<String> get() {
        try {
            Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

            DataFlavor dataFlavor = DataFlavor.stringFlavor;

            if (systemClipboard.isDataFlavorAvailable(dataFlavor)) {
                Object text = systemClipboard.getData(dataFlavor);
                return Optional.of(text.toString());
            }
        } catch (Exception e) {
        }
        return Optional.empty();
    }

}
