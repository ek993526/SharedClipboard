package com.ek.service;

import com.ek.model.EncryptedData;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.tuple.Pair;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.Random;


public class EncryptionProvider {

    public static final int ITERATION_COUNT = 65536;
    private final Cipher rsaCipher;
    private final KeyPairGenerator rsa;

    private final Cipher passwordCipher;
    private final SecretKeyFactory keyFactory;


    @SneakyThrows
    public EncryptionProvider() {
        String RSA = "RSA";

        passwordCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        rsa = KeyPairGenerator.getInstance(RSA);
        rsaCipher = Cipher.getInstance(RSA);
        rsa.initialize(2048);

    }

    public String decrypt(Object key, byte[] inputFile, boolean isPassword) {
        String result;

        if (key == null) {
            result = new String(inputFile, StandardCharsets.UTF_8);
        } else if (isPassword) {
            var encryptedFile = (EncryptedData) readObject(inputFile);
            result = new String(decrypt(encryptedFile, ((char[]) key)));
        } else {
            var encryptedFile = (EncryptedData) readObject(inputFile);
            result = new String(decrypt(encryptedFile, ((KeyPair) key)));
        }

        return result.replace(System.lineSeparator(), "");
    }

    public KeyPair generateKeyRSA() {
        return rsa.generateKeyPair();
    }


    @SneakyThrows
    public EncryptedData encrypt(byte[] data, KeyPair key) {
        String symmetricKey = RandomStringUtils.randomAlphanumeric(245);
        var encryptedFile = encrypt(data, symmetricKey.toCharArray());
        rsaCipher.init(Cipher.ENCRYPT_MODE, key.getPublic());
        return EncryptedData.builder()
                .additionalData(rsaCipher.doFinal(symmetricKey.getBytes()))
                .data(toBytes(encryptedFile))
                .build();
    }

    @SneakyThrows
    private byte[] toBytes(EncryptedData encryptedFile) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(encryptedFile);
        oos.flush();
        return bos.toByteArray();
    }

    @SneakyThrows
    public byte[] decrypt(EncryptedData data, KeyPair key) {
        rsaCipher.init(Cipher.DECRYPT_MODE, key.getPrivate());
        String symmetricKey = new String(rsaCipher.doFinal(data.getAdditionalData()));
        return decrypt((EncryptedData) readObject(data.getData()), symmetricKey.toCharArray());
    }

    @SneakyThrows
    public EncryptedData encrypt(byte[] data, char[] password) {
        byte[] salt = new byte[8];
        Random random = new Random();
        random.nextBytes(salt);

        PBEKeySpec keySpec = new PBEKeySpec(password, salt, ITERATION_COUNT, 256);

        SecretKey tmp = keyFactory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        passwordCipher.init(Cipher.ENCRYPT_MODE, secret);
        AlgorithmParameters params = passwordCipher.getParameters();
        byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        return EncryptedData.builder()
                .data(passwordCipher.doFinal(data))
                .additionalData(SerializationUtils.serialize(Pair.of(iv, salt)))
                .build();
    }

    @SneakyThrows
    public byte[] decrypt(EncryptedData data, char[] password) {
        Pair<byte[], byte[]> ivAndSalt = (Pair<byte[], byte[]>) SerializationUtils.deserialize(data.getAdditionalData());
        var iv = ivAndSalt.getKey();
        var salt = ivAndSalt.getValue();

        PBEKeySpec keySpec = new PBEKeySpec(password, salt, ITERATION_COUNT, 256);

        SecretKey tmp = keyFactory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        passwordCipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
        return passwordCipher.doFinal(data.getData());
    }


    private Object readObject(byte[] inputFile) {
        try (var file = new ObjectInputStream(new ByteArrayInputStream(inputFile))) {
            return file.readObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
