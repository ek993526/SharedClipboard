package com.ek.service;

import com.dropbox.core.*;
import com.dropbox.core.oauth.DbxCredential;
import com.dropbox.core.oauth.DbxRefreshResult;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import lombok.SneakyThrows;

import java.io.ByteArrayInputStream;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class DropBoxInterface {

    public static final String CLIPBOARD = "/clipboard";
    private DbxClientV2 client;


    @SneakyThrows
    public DropBoxInterface(String appKey, String appSecret) {
        // DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/ek-clipboard").build();
        // client = new DbxClientV2(config, apiKey);


        // Створюємо конфігурацію для Dropbox API

        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/ek-clipboard")
                .withUserLocale(Locale.getDefault().toString()).build();
        DbxAppInfo appInfo = new DbxAppInfo(appKey, appSecret);

        // Отримуємо авторизаційну URL для отримання коду авторизації
        DbxWebAuth auth = new DbxWebAuth(config, appInfo);
        String authorizeUrl = auth.authorize(DbxWebAuth.newRequestBuilder()
                .withNoRedirect()
                .withTokenAccessType(TokenAccessType.OFFLINE)
                .build());

        // Виводимо URL для авторизації
        System.out.println("1. Go to: " + authorizeUrl);
        System.out.println("2. Click \"Allow\" (you might have to log in first)");
        System.out.println("3. Copy the authorization code.");

        // Очікуємо введення коду авторизації
        String code = new java.util.Scanner(System.in).nextLine().trim();

        // Обмін коду авторизації на токен доступу
        DbxAuthFinish authFinish = auth.finishFromCode(code);

        client = new DbxClientV2(config, new DbxCredential(authFinish.getAccessToken(), authFinish.getExpiresAt(), authFinish.getRefreshToken(), appKey, appSecret));

        FullAccount account = client.users().getCurrentAccount();
        System.out.println("Logged in as: " + account.getName().getDisplayName());

        try {
            client.files().createFolderV2(CLIPBOARD);
        } catch (Exception ignored) {
        }

        new Thread(() -> {
            var sleepTime = authFinish.getExpiresAt() - System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5);
            System.out.println("Sleep time: " + sleepTime + " ms, " + sleepTime / 1000 + " seconds " + sleepTime / 1000 / 60 + " minutes");
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(sleepTime);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                try {
                    System.out.println("refreshing access token");
                    DbxRefreshResult refreshResult = client.refreshAccessToken();
                    client = new DbxClientV2(config, new DbxCredential(refreshResult.getAccessToken(), refreshResult.getExpiresAt(), authFinish.getRefreshToken(), appKey, appSecret));
                } catch (DbxException e) {
                    System.out.println("failed to refresh access token");
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }

    public Optional<byte[]> retrieveClipboard(long version) {
        return get(String.valueOf(version));
    }

    public void saveToClipboard(byte[] data, long version) {
        save(data, String.valueOf(version));
    }

    @SneakyThrows
    public void save(byte[] data, String key) {
        try (var upload = client.files().upload(CLIPBOARD + "/" + key)) {
            upload.uploadAndFinish(new ByteArrayInputStream(data));
        } catch (Exception ignored) {
        }
    }

    @SneakyThrows
    public Optional<byte[]> get(String key) {
        try (var inputStream = client.files().download(CLIPBOARD + "/" + key)) {
            return Optional.of(inputStream.getInputStream().readAllBytes());
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    @SneakyThrows
    public long getNextVersion() {
        return client.files().listFolder(CLIPBOARD).getEntries().size();
    }
}
