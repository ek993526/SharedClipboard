package com.ek;

import com.ek.model.EncryptedData;
import com.ek.service.ClipboardService;
import com.ek.service.DropBoxInterface;
import com.ek.service.EncryptionProvider;
import com.ek.utils.Common;
import lombok.SneakyThrows;

import java.io.File;
import java.security.KeyPair;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    public static final String KEY = "key";
    private static final ClipboardService clipboardService = new ClipboardService();
    private static final EncryptionProvider encryptionProvider = new EncryptionProvider();
    private static DropBoxInterface dropBoxInterface;

    @SneakyThrows
    public static void main(String[] args) {

        var key = Common.getHiddenData("Enter dropbox App key ");
        var secret = Common.getHiddenData("Enter dropbox App secret: ");

        dropBoxInterface = new DropBoxInterface(new String(key), new String(secret));
        long latestIndex = dropBoxInterface.getNextVersion();
        String latestClipboard = "";

        completeHandshake();

        clipboardService.set(""); // clear
        //noinspection InfiniteLoopStatement
        while (true) {
            Optional<String> s = clipboardService.get();

            try {
                if (s.isPresent() && !s.get().equals(latestClipboard)) {
                    System.out.println("Sharing new clipboard");
                    var data = encryptionProvider.encrypt(s.get().getBytes(), getKey());
                    dropBoxInterface.saveToClipboard(Common.writeObject(data), latestIndex++);
                    latestClipboard = s.get();
                } else {
                    long nextVersion = dropBoxInterface.getNextVersion();
                    if (latestIndex != nextVersion) {
                        Optional<byte[]> data = dropBoxInterface.retrieveClipboard(nextVersion - 1);
                        if (data.isPresent()) {
                            System.out.println("Retrieved new clipboard");
                            EncryptedData encryptedData = Common.readObject(data.get(), EncryptedData.class);
                            byte[] decrypted = encryptionProvider.decrypt(encryptedData, getKey());
                            String newClipboard = new String(decrypted);
                            clipboardService.set(newClipboard);
                            latestClipboard = newClipboard;
                            latestIndex = nextVersion;
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
                System.out.println("Trying to ignore error and retry");
            }

            TimeUnit.SECONDS.sleep(1);
        }
    }

    private static void completeHandshake() {
        if(new File(KEY).exists()){
            return;
        }

        System.out.println("How do you proceed with handshake?");
        System.out.println("1) Generate new key encrypted by password");
        System.out.println("2) Use existing remove key encrypted by password");

        Scanner in = new Scanner(System.in);
        byte b = in.nextByte();

        if (b == 1) {
            KeyPair keyPair = encryptionProvider.generateKeyRSA();
            Common.writeObject(KEY, keyPair);

            char[] password = Common.getHiddenData("Enter password to encrypt handshake key: ");
            EncryptedData encrypted = encryptionProvider.encrypt(Common.writeObject(keyPair), password);

            dropBoxInterface.save(Common.writeObject(encrypted), "key");
        } else if (b == 2) {
            var data = dropBoxInterface.get("key").orElseThrow(() -> new RuntimeException("No key found on remote"));
            char[] password = Common.getHiddenData("Enter password to encrypt handshake key: ");

            byte[] decrypted = encryptionProvider.decrypt(Common.readObject(data, EncryptedData.class), password);
            KeyPair keyPair = Common.readObject(decrypted, KeyPair.class);
            Common.writeObject(KEY, keyPair);
        }

        if (!new File(KEY).exists()) {
            throw new RuntimeException("Handshake failed!");
        }
    }

    private static KeyPair getKey() {
        return Common.readObject(KEY, KeyPair.class);
    }


}